# Dead Simple Sharing Plugin

A WordPress plugin that does dead (dead!) simple sharing buttons. No SDKs, no JS bloat.

### Installation

Grab the latest version here: 

https://bitbucket.org/relishinc/wordpress-dead-simple-sharing-plugin/get/HEAD.zip

Unzip it and put it in your ```/wp-content/plugins``` folder. Activate the plugin and go. There are no settings.

### Usage

This plugin creates a shortcode that in its simplest form can be used like this in the text editor:

```php
[dss]
```

Or in a template file like this:

```php
<?php echo do_shortcode('[dss]'); ?>
```

Default sharing platforms are:

* Twitter
* Facebook
* Google+
* LinkedIn
* Email

There are a couple of options available in the shortcode (all are optional):

```
[dss include="twitter" exclude="googleplus" labels="false" url="http://example.com" title="A great page"]
```

* `include` list only the platforms you want to include (comma separated)
* `exclude` list the platforms you want to exclude (comma separated)
* `labels` set to "0" or false to hide the button labels
* `url` specify a URL (otherwise the current page's URL gets used)
* `labels` specify a title (otherwise the current page's title gets used)