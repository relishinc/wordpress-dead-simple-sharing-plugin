<?php
/*
   Plugin Name: Dead Simple Sharing
   Plugin URI: http://relishinc.bitbucket.io
   Version: 0.1.2
   Author: Steve Palmer / Relish
   Author URI: https://reli.sh
   Description: The most simple (and nicest looking) sharing buttons you can possibly use. No SDKs, no JS libraries - just a bit of Javscript-iness to do sharing popups.
   Text Domain: dead-simple-sharing
   License: GPLv3
  */
  

add_action( 'init', [ 'DeadSimpleSharing', 'init' ], 0 );

class DeadSimpleSharing {
  
  protected $options = [
    'container_class' => 'ds-sharing',
  ];

  public static function init()
  {
    $class = __CLASS__;
    new $class;
  }

  public function __construct() 
  {
    add_action( 'wp_enqueue_scripts', [ $this, 'initPluginScripts' ] );
    add_shortcode( 'dss', [ $this, 'getShortcode' ] );
  }

  public function initPluginScripts() 
  {
    // css
    
    wp_register_style( 'dss-styles', plugin_dir_url( __FILE__ ) . 'assets/css/share.css');
    wp_enqueue_style( 'dss-styles' );
    
    // js
    wp_register_script( 'dss-scripts', plugin_dir_url( __FILE__ ) . 'assets/js/share.js', [ 'jquery' ], false, true );
    wp_enqueue_script( 'dss-scripts' ); 

    wp_add_inline_script(
      'dss-scripts',
      '
        DEAD_SIMPLE_SHARE.init({
          containerClass:  "' . $this->options['container_class'] . '"
        });
      '
    );
  
  }
  
  public function getShortcode( $atts )
  {
    return $this->getSharingHtml( $atts );
  }
  
  protected function getSharingHtml( $options )
  {
    $templates = [
      'twitter'     => '<a href="https://twitter.com/share?url={url}&text={text}&hashtags={hashtags}' . ( isset($options['twitter-user']) ? '&via=' . $options['twitter-user'] : '' ) . '" class="share-button" data-platform="twitter" target="_blank" role="button" aria-label="Share on Twitter"><span class="button-icon">' . file_get_contents( plugin_dir_path( __FILE__ ) . 'assets/svg/twitter.svg' ) . '</span> <span class="button-label">Share on Twitter</span></a>',
      'facebook'    => '<a href="http://www.facebook.com/sharer.php?u={url}" class="share-button" data-platform="facebook" target="_blank" role="button" aria-label="Share on Facebook"><span class="button-icon">' . file_get_contents( plugin_dir_path( __FILE__ ) . 'assets/svg/facebook.svg' ) . '</span> <span class="button-label">Share on Facebook</span></a>',
      'googleplus'  => '<a href="https://plus.google.com/share?url={url}" class="share-button" data-platform="googleplus" target="_blank" role="button" aria-label="Share on Google+"><span class="button-icon">' . file_get_contents( plugin_dir_path( __FILE__ ) . 'assets/svg/google-plus.svg' ) . '</span> <span class="button-label">Share on Google+</span></a>',
      'linkedin'    => '<a href="http://www.linkedin.com/shareArticle?mini=true&url={url}" class="share-button" data-platform="linkedin" target="_blank" role="button" aria-label="Share on LinkedIn"><span class="button-icon">' . file_get_contents( plugin_dir_path( __FILE__ ) . 'assets/svg/linkedin.svg' ) . '</span> <span class="button-label">Share on LinkedIn</span></a>',
      'email'       => '<a href="mailto:?subject={text}&body={text}%0D%0A%0D%0A{url}" class="share-button" data-platform="email" target="_blank" role="button" aria-label="Share via Email"><span class="button-icon">' . file_get_contents( plugin_dir_path( __FILE__ ) . 'assets/svg/mail.svg' ) . '</span> <span class="button-label">Share via Email</span></a>',
    ];
    $include = isset($options['include']) ? explode(',', $options['include']) : false;
    $exclude = isset($options['exclude']) ? explode(',', $options['exclude']) : false;
    $classes = [];
    $buttons = [];
    
    // add css classes based on options
    
    if ( isset($options['labels']) && ( ! (bool) $options['labels'] || $options['labels'] == 'false' ) )
    {
      $classes[] = 'no-labels';
    }
    
    // add links
    
    foreach ( $templates as $platform => $template )
    {
      $display = true;
      
      if ( $exclude && in_array( $platform, $exclude ) )
      {
        $display = false;
      }
      
      if ( $include && ! in_array( $platform, $include ) )
      {
        $display = false;
      }
        
      if ( $display )
      {  

        if ( $options['url'] )
        {
          $template = str_replace('{url}', urlencode($options['url']), $template);
        }

        if ( $options['title'] )
        {
          $template = str_replace('{text}', urlencode($options['title']), $template);
        }        

        $buttons[] = $template;
      }
      
    }
    
    return sprintf( '<div class="' . $this->options['container_class'] . ' ' . implode( ' ', $classes ) . '">%1$s</div>', implode('', $buttons) );
    
  }
  
}