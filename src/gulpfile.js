var 
  gulp            = require('gulp'),
  gutil           = require('gulp-util'),
  bower           = require('bower'),
  concat          = require('gulp-concat'),
  header          = require('gulp-header'),
  minifyCss       = require('gulp-minify-css'),
  rename          = require('gulp-rename'),
  sh              = require('shelljs'),
  less            = require('gulp-less'),
  uglify          = require('gulp-uglify'),
  sourcemaps      = require('gulp-sourcemaps'),
  livereload      = require('gulp-livereload')
  ;

var 
  paths = {
    less:         ['less/**/*.less'],
    scripts:      ['js/**/*.js'],
    images:       ['svg/**/*']
  },
  dest    = './../',
  banner  = '/* Generated <%= (new Date()).toUTCString() %> */\n';

gulp.task('css', function(done) {
  
  // main css
  
  return gulp
    .src([
      'less/main.less'
    ])
    .pipe(less())
    .pipe(rename('share.css'))
    .pipe(header(banner))
    .pipe(gulp.dest(dest + 'assets/css'))
    .pipe(livereload());
        
});

gulp.task('scripts', function() {

  // main js

  return gulp
    .src([
      'js/main.js'
    ])
    .pipe(concat('share.js'))
    .pipe(header(banner))
    .pipe(gulp.dest(dest + 'assets/js'))
    .pipe(livereload());
  
});

gulp.task('images', function(done) {
  
  return gulp
  	.src(paths.images)
		.pipe(gulp.dest(dest + 'assets/svg'));  		

});

gulp.task('watch', function() {
  
  livereload.listen();

  gulp.watch('gulpfile.js', [ 'css', 'scripts' ]);
  gulp.watch(paths.less, ['css']);
  gulp.watch(paths.scripts, ['scripts']);
  gulp.watch(paths.images, ['images']);      
  
});

gulp.task('default', [ 'css', 'scripts', 'images', 'watch' ], function() {  
});
