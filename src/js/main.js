var DEAD_SIMPLE_SHARE = ( function ($) {
  
  var _init = function ( $options ) 
  {
    DEAD_SIMPLE_SHARE.options = $options || {};
    
    jQuery(document)
      .ready(function()
      {
        _initShareButtons();
      });
      
    jQuery(document)
      .on('pjax:end', function() 
      {
        _initShareButtons();
      }); 
  };

  var _initShareButtons = function()
  {
    var
      url             = window.location.href,
      text            = document.title,
      hashtags        = '',
      windowSettings  = 'width=540,height=400,resizable=yes,scrollbars=yes,centerscreen,chrome=yes';
    
    $('.' + DEAD_SIMPLE_SHARE.options.containerClass)
      .find('.share-button')
      .each(function() {
        var
          href = $(this).attr('href');
        
        $(this)
          .attr('href', href.replace( /{url}/g, encodeURIComponent(url) ).replace( /{text}/g, encodeURIComponent(text) ).replace( /{hashtags}/g, encodeURIComponent(hashtags) )); 
          
      })
      .on('click', function(e) {
        
        // in case they've changed

        url  = window.location.href,
        text = document.title,
        href = $(this).attr('href');
        
        $(this)
          .attr('href', href.replace( /{url}/g, encodeURIComponent(url) ).replace( /{text}/g, encodeURIComponent(text) ).replace( /{hashtags}/g, encodeURIComponent(hashtags) )); 
        
        switch( $(this).data('platform') )
        {
          case 'twitter':
          case 'facebook':
          case 'googleplus':
          case 'linkedin':
            e.preventDefault();
            window.open($(this).attr('href'), $(this).data('platform') + '-sharing', windowSettings);
            break;
        }
        
      });
  };

  return {
    init: _init
  }

} )(jQuery);